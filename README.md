# Laravel test API

### Developed by UFO Engineering
[![N|Solid](https://s.dou.ua/CACHE/images/img/static/companies/%D0%B2%D0%B2/62de7518b531c2e80eaa87fe8ff6f1f4.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Installation

```sh
$ clone project
$ cd testing
$ cp .env.example .env
$ composer install
$ npm i
$ npm run dev
$ php artisan migrate
$ cp property-data.csv storage/app/property-data.csv
$ php artisan import:property (to import properties from storage/app/property-data.csv)
$ php artisan serve
```

API domain: http://127.0.0.1:8000

### Api
GET /api/property

example with params:
http://127.0.0.1:8000/api/property?name=to&bathrooms=2&price[]=370000&price[]=500000&bedrooms=1&bathrooms=2&storeys=2&garages=2

(notice that price will apply if the price range exists)
