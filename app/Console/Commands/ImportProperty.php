<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Imports\PropertyImport;
use Excel;

class ImportProperty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import property data to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Excel::import(new PropertyImport, 'property-data.csv', 'local');
            $this->info('Property data was succesfully imported.');
        } catch (\Exception $e) {
            $this->error("Import error: {$e->getMessage()}");
        }
    }
}
