<?php

namespace App\Imports;

use App\Models\Property;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;

class PropertyImport implements OnEachRow, WithHeadingRow
{
    /**
     * Insert only new record
     * @param Maatwebsite\Excel\Row
     *
     * @return void
     */
    public function onRow(Row $row) : void
    {
        $row = $row->toArray();
        Property::updateOrCreate(
            ['name' => $row['name']],
            $row
        );
    }
}
