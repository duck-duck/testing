<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class PropertyFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];


    public function name(string $value)
    {
        return $this->whereLike('name', $value);
    }

    /**
    * Apply price if the price range exists OR skip
    * @param mix $value
    */
    public function price($value)
    {
        $query = $this;

        if (is_array($value) && count($value) >= 2) {
            $query = $this->where('price', ">", $value[0])->where('price', "<", $value[1]);
        }

        return $query;
    }

    public function bedrooms($value)
    {
        return $this->where('bedrooms', $value);
    }

    public function bathrooms($value)
    {
        return $this->where('bathrooms', $value);
    }

    public function storeys($value)
    {
        return $this->where('storeys', $value);
    }

    public function garages($value)
    {
        return $this->where('garages', $value);
    }
}
